import React from 'react';
import ReactTable from "react-table";
import "react-table/react-table.css";
import {Container, Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input} from 'reactstrap';
import { MaterialPicker } from 'react-color';
import $ from 'jquery';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      name: '',
      code: '',
      profession: '',
      city: '',
      branch: '',
      assigned: false,
      colour: '#FFFFFF',
      employees: []
    };
    this.toggle = this.toggle.bind(this);
  }
  componentWillMount = () => {
    fetch('http://localhost:8080/api/employees')
      .then(response => response.json())
      .then(employees => this.setState({ employees }))
  }

  toggle() {
    if (this.state.modal){
      $.ajax({
        url: 'http://localhost:8080/api/employees',
        method: 'POST',
        data: this.constructEmployeeObject(),
        success: (data) => {
          this.setState({employees : data})
        } 
      })
    }
    this.setState({
      modal: !this.state.modal
    });
  }

  delete(id, e) {
    $.ajax({
      url: 'http://localhost:8080/api/employees',
      method: 'DELETE',
      data: id,
      success: (data) => {
        this.setState({employees : data})
      } 
    })
  }
  handleChangeComplete = (color, event) => {
    this.setState({ colour: color.hex });
  };

  constructEmployeeObject() {
    let newEmployee = {}
    newEmployee.name = document.getElementById('name').value
    newEmployee.code = document.getElementById('code').value
    newEmployee.profession = document.getElementById('profession').value
    newEmployee.color = this.state.colour
    newEmployee.city = document.getElementById('city').value
    newEmployee.branch = document.getElementById('branch').value
    newEmployee.assigned = $('#assigned').is(':checked')

    return newEmployee
  }



  render() {
    const employees = this.state.employees;
    const onRowClick = (state, rowInfo, column, instance) => {
      return {
          onClick: e => {
              // This shows up as undefined so I had to use the rowInfo as a starting point and check if the index of the clicked cell was the last one (8). If it is equal to 8, I call the delete function with the id being sent as the parameter. https://stackoverflow.com/questions/43112105/react-table-component-onclick-event-for-a-column

              console.log('It was in this column:', column)
              if(rowInfo.index === 8){
                this.delete(rowInfo.original.id)
              }
          }
      }
  }
    const columns = [
      {Header: 'ID', accessor: 'id'},
      {Header: 'Name', accessor: 'name'},
      {Header: 'Code', accessor: 'code'},
      {Header: 'Profession', accessor: 'profession'},
      {Header: 'Color', accessor: 'color', Cell: props => <span style={{backgroundColor: props.value, color: 'white', width: '100%', display: 'block', textAlign: 'center', textTransform: 'uppercase'}}>{props.value}</span>},
      {Header: 'City', accessor: 'city'},
      {Header: 'Branch', accessor: 'branch'},
      {Header: 'Assigned', accessor: 'assigned', Cell: props => <input type="checkbox" defaultChecked={props.value} />},
      {Header: 'Actions', accessor: 'id', Cell: props => <Button color="secondary" onClick={(e) => this.delete(props.value, e)}>Delete</Button> }
    ]
    console.log(this.state);

    return (
      <div className="App">
        <center><h1>Plexxis Employees</h1></center>
        <Container>
          <ReactTable data={employees} columns={columns} defaultPageSize={10} getTrProps={onRowClick} />
          <br/>
          <div>
            <Button color="primary" onClick={this.toggle}>Add</Button>
          </div>
        </Container>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <Form>
            <ModalHeader toggle={this.toggle}>
              Add An Employee
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="name">Name</Label>
                <Input type="text" name="name" id="name" placeholder="Employee Name" />
              </FormGroup>
              <FormGroup>
                <Label for="code">Code</Label>
                <Input type="text" name="code" id="code" placeholder="Employee Code" />
              </FormGroup>
              <FormGroup>
                <Label for="profession">Profession</Label>
                <Input type="text" name="profession" id="profession" placeholder="Employee Profession"  />
              </FormGroup>
              <FormGroup className="colour-picker-block">
                <Label for="colour">Colour</Label>
                <MaterialPicker color={ this.state.colour } onChangeComplete={ this.handleChangeComplete } />
              </FormGroup>
              <FormGroup>
                <Label for="city">City</Label>
                <Input type="text" name="city" id="city" placeholder="Employee City" />
              </FormGroup>
              <FormGroup>
                <Label for="city">Branch</Label>
                <Input type="text" name="branch" id="branch" placeholder="Employee Branch" />
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input type="checkbox" name="assigned" id="assigned" />{' '}Assigned
                </Label>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Add Employee</Button>{' '}
              <Button color="secondary" onClick={this.toggle}>Cancel</Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default App;
