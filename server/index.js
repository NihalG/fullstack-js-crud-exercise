const express = require('express')
const cors = require('cors')
const bodyParser = require("body-parser");
const app = express()
let employees = require('./data/employees.json')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.options('/api/employees/', cors()) // enable pre-flight request for DELETE request


var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}

app.get('/api/employees', cors(corsOptions), (req, res, next) => {
  console.log('Displaying Employees');
  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify(employees, null, 2));
})

app.post('/api/employees', cors(corsOptions), (req, res, next) => {
  console.log('Adding Employees');
  req.body.id = employees[employees.length - 1].id + 1;
  employees.push(req.body)
  console.log(employees);
  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify(employees, null, 2));
})

app.delete('/api/employees', cors(corsOptions), (req, res, next) => {
  console.log('Deleting Employees');
  employees.splice(employees.indexOf(employees.find(employee => employee.id === req.body.id)), 1)
  console.log(employees);
  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify(employees, null, 2));
})

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'))